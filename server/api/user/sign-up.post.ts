interface IBody {
    firstName : string
    lastName: string
    email: string
    password: string
}

export default defineEventHandler<{body : IBody, query: { id: string}}>(async(event) => {
    // const { email } = await readBody(event);
    // const files = await readMultipartFormData(event);
    // const result: string[] = []
    // files?.forEach((file) => {
    //     result.push(file.filename!)
    // })
    // return result;
    const query  = getQuery(event)
    return query;
})